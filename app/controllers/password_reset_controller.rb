class PasswordResetController < ApplicationController
  
  layout 'basic'
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  include SessionsHelper
  
  def new
  end
  
  def create 
    @user = User.find_by_email(params[:password_reset][:email].downcase)
    unless @user
      flash.now[:warning] = 'That email address does not exist.'
      render('new')      
    else
      @user.create_reset_digest
      flash[:notice] = "Check your email for password reset instructions."
      UserMailer.password_reset(@user).deliver_now
      redirect_to(root_url)
    end
  end
  
  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, "cannot be empty.")
      render('edit')
    elsif @user.update_attributes(user_params)
      log_in(@user)
      flash[:notice] = "Password has been reset."
      redirect_to(@user)
    else
      render('edit')
    end
  end

  def edit
  end
  
private

  def get_user
    @user = User.find_by_email(params[:email])
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def valid_user
    unless(@user && @user.activated? && @user.authenticated?(:reset, params[:id]))
      flash[:warning] = "Something went horribly wrong.  Just wrong."
      redirect_to(root_url)
    end
  end
  
  def check_expiration
    if @user.password_reset_expired?
      flash[:warning] = "Password reset has expired."
      redirect_to(new_password_reset_url)
    end
  end
  
end
