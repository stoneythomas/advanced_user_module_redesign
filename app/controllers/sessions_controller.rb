class SessionsController < ApplicationController
  
  layout 'basic'
  include SessionsHelper
  
  def new
    
  end
  
  def create
    user = User.find_by_email(params[:session][:email].downcase)
    
    unless user && user.authenticate(params[:session][:password])
      flash.now[:warning] = "<li>That was not the correct email/password combination.</li>"
      render('new') and return false
    end
    
    if user.activated?
      log_in(user)
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to(user)
    else
      flash[:warning] = "<li>This account is not activated.  Check your email for the activation link.</li>"
      flash[:warning] << "<li>#{view_context.link_to("Send me a new activation link", resend_activation_user_url(user))}</li>"
      redirect_to(root_url)
    end
  end
  
  def destroy
    log_out if logged_in?
    flash[:notice] = "<li>You are logged out.</li>"
    redirect_to(root_url)
  end
  
end
