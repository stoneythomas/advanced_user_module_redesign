class UsersController < ApplicationController
  
  layout 'basic'
  include ApplicationHelper
  include SessionsHelper
  before_action :has_permission, :only => [:edit, :update, :delete, :destroy, :crop_avatar]
  before_action :remember_location, :only => [:index, :show]
  
    
  def index
    if logged_in?
      @users = User.where(:activated => true).order("name ASC").paginate(page: params[:page], :per_page => 6)
    else
      flash[:warning] = "<li>You must be logged in to see that page.</li>"
      redirect_to(root_url)
    end
  end
  
  def show
    unless @user = User.find_by_id(params[:id])
      flash[:warning] = "<li>That user does not exist.</li>"
      redirect_to(root_path)
    end
    unless @user.activated?
      flash[:warning] = "<li>That user is not activated.</li>"
      redirect_to(users_url(page: params[:page]))
    end    
  end
  
  def new
    @user = User.new
    @heading = "Register here"
    @form_submit = "Submit registration"
  end  
    
  def create
    @user = User.new(user_params)
    @user.role = 'common'
    
    unless @user.save
      compile_errors(@user, "now", "warning")
      render('new') and return false
    end
    
    UserMailer.account_activation(@user).deliver_now
    flash[:notice] = "Please check your email to activate your account."
    redirect_to(root_url)
  end
  
  def resend_activation
    @user = User.find(params[:id])
    unless @user.activated      
      @user.reset_activation_digest
      UserMailer.account_activation(@user).deliver_now
      flash[:notice] = "Your activation email has been sent again. Please check your junk folder if you cannot find it."
      redirect_to(root_url)  
    else
      flash[:warning] = "That account has already been activated."
      redirect_to(root_url)      
    end
  end
  
  def edit
    unless @user = User.find(params[:id])
      flash[:warning] = "<li>That user does not exist.</li>"
      redirect_to(root_path)
    end
    @heading = "Edit your data"
    @form_submit = "Save your changes"
  end
  
  def crop_avatar
    unless @user = User.find(params[:id])
      flash[:warning] = "<li>That user does not exist.</li>"
      redirect_to(root_path)
    end
    if @user.avatar_file_name.blank?
      flash[:warning] = "<li>That user does not have an avatar.</li>"
      redirect_to(@user)
    end
  end
  
  def update
    @user = User.find(params[:id])
    
    if params[:user][:password].blank?
      params[:user].delete :password
      params[:user].delete :password_confirmation
    end
    
    unless @user.update_attributes(user_params)
      compile_errors(@user, "now")
      render('edit') and return false
    end
    
    flash[:notice] = "#{@user.name} has been updated successfully."
    redirect_to(@user)
  end
  
  def delete
    @user = User.find(params[:id])
  end
  
  def destroy
    user = User.find(params[:id])
    log_out unless is_admin?
    user.destroy
    private_messages_to_destroy = PrivateMessage.where(sender_id: params[:id])
    private_messages_to_destroy.each do |pm|
      pm.destroy
    end
    flash[:notice] = "User '#{user.name}' has been destroyed."
    redirect_to(root_url)
  end
  
  
  private
  
    def user_params
      params.require(:user).permit(:name, :password, :password_confirmation, :email, :biography, :avatar, :avatar_original_w, :avatar_original_h, :avatar_crop_x, :avatar_crop_y, :avatar_crop_w, :avatar_crop_h)
    end 
    
    def has_permission
      unless is_self?(params[:id].to_i) || is_admin?
        flash[:warning] = "<li>You do not have permission to #{params[:action]} that.</li>"
        redirect_to(root_url)
      end
    end  
  

end
