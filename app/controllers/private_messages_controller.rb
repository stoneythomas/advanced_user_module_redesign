class PrivateMessagesController < ApplicationController
  
  include ApplicationHelper
  include SessionsHelper
  require 'will_paginate/array' 
  before_action :need_to_login?
  before_action :remember_location, :only => [:index]
  layout 'basic'
  
  def index
    @private_messages_raw = PrivateMessage.where("user_id = ?", session[:user_id]).order('created_at DESC')
    @private_messages_new = Array.new
    @private_messages_old = Array.new
    @private_messages_raw.each do |pm|
      pm.read_date? ? @private_messages_old << pm : @private_messages_new << pm
    end
    @private_messages = @private_messages_new + @private_messages_old 
    @private_messages = @private_messages.paginate(:per_page => 5, :page => params[:page])
  end
  
  def show
    @private_message = PrivateMessage.find(params[:id])
    unless @private_message.user_id == session[:user_id]
      flash[:warning] = "That is someone else's message!"
      redirect_to(private_messages_url) and return false
    end
    @private_message.update_attribute(:read_date, Time.zone.now)
  end
  
  def new
    unless params[:private_message][:user_id].present?
      flash[:warning] = "<li>You cannot send a message with no recipient.</li>"
      redirect_to(session[:location]) and return false
    end
    @private_message = PrivateMessage.new
    @private_message.user_id = params[:private_message][:user_id]
    @private_message.reply_id = params[:private_message][:reply_id]
    unless user_id_valid?(@private_message.user_id)
      flash[:warning] = "<li>You cannot send a message to a user that does not exist.</li>"
      redirect_to(session[:location])
    end
    @private_message = load_original_message_text(@private_message) unless @private_message.reply_id.nil?
  end
 
  def create
    @private_message = PrivateMessage.new(message_params)
    @private_message.sender_id = session[:user_id]
    unless @private_message.save
      compile_errors(@private_message, "now", "warning")
      render('new') and return false
    end
    @recipient = User.find(params[:private_message][:user_id])
    UserMailer.private_message(@recipient, @private_message).deliver_now
    flash[:notice] = "<li>Your message has been sent to #{User.find(@private_message.user_id).name}.</li>"
    redirect_to(session[:location])
  end
  
  def unread
    unless @private_message = PrivateMessage.find(params[:id])
      flash[:warning] = "<li>That message does not exist.</li>"
      redirect_to(session[:location]) and return false
    end
    @private_message.update_attribute(:read_date, nil)
    redirect_to(session[:location])
  end
  
  def delete
    @private_message = PrivateMessage.find(params[:id])
  end
  
  def destroy
    private_message = PrivateMessage.find(params[:id])
    private_message.destroy
    flash[:notice] = "A message has been destroyed."
    redirect_to(session[:location])
  end  
  
  
  private
  
    def message_params
      params.require(:private_message).permit(:subject, :message, :user_id, :reply_id)
    end 
    
    def need_to_login?
      unless logged_in?
        flash[:warning] = '<li>You must be logged in for private message features to work.</li>'
        redirect_to(login_url) and return false
      end
    end

    def user_id_valid?(user_id)
      return true unless User.find_by_id(user_id).nil?
    end
    
    def load_original_message_text(private_message)
      reply_text = PrivateMessage.find_by_id(private_message.reply_id)
      unless reply_text.nil?
        private_message.message = reply_text.message.prepend("\n\n==== REPLYING TO ====\n")
        private_message.subject = reply_text.subject.prepend("RE: ").gsub(/(RE: )+/, 'RE: ')
      end
      return private_message
    end
    
end
