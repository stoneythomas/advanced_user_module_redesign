class AccountActivationsController < ApplicationController
    
  include SessionsHelper
    
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
        user.update_attribute(:activated, true)
        user.update_attribute(:activated_at, Time.zone.now)
        user.update_attribute(:activation_digest, nil)
        log_in(user)
        flash[:notice] = "Your account has been successfully activated."
        redirect_to(user)
        
    else
      flash[:warning] = "That was an invalid activation link."
      redirect_to(root_url)
    end
  end
  
end
