$(document).on('turbolinks:load', function() {

    /* EXPAND/CONTRACT COLLAPSE MENU */
    var navigationState = 'closed';
    var navigationUsed = 'no';
    $("#menu-expander").click(function(){
        navigationUsed = 'yes';
        if (navigationState == 'closed') {
            $(".screen-top-navigation ul").slideDown(1000);
            navigationState = 'open';
        }
        else {
            $(".screen-top-navigation ul").slideUp(1000);
            navigationState = 'closed';
        }
    });
    
    $( window ).resize(function() {
        if (navigationUsed == 'yes') {
            $(".screen-top-navigation ul").show();
            navigationState = 'open';
        }
    });

});