module ApplicationHelper
    
  private
  
  # TAKES ARGUMENT 1 AND FORMATS THE ERROR MESSAGES INTO AN HTML LIST IN FLASH
  # ARGUMENT 2 SPECIFIES IF THE FLASH IS TO DISPLAY ON RENDER OR REDIRECT
  # ARGUMENT 3 DECIDES WHICH FLASH KEY TO USE FOR DIFFERENT DISPLAY STYLES
  def compile_errors(user, state = "new-request", type = "notice")
    
    # Gets rid of annoying duplicate error messages from image upload
    user.errors.delete :avatar
    
    if state == 'now' && type == 'notice'
      flash.now[:notice] = ''
      user.errors.full_messages.each do |message|
        flash.now[:notice] << "<li>"+message+"</li>"
      end
      
    elsif state == 'new-request' && type == 'notice'
      flash[:notice] = '' 
      user.errors.full_messages.each do |message|
        flash[:notice] << "<li>"+message+"</li>"
      end
      
    elsif state == 'now' && type == 'warning'
      flash.now[:warning] = ''
      user.errors.full_messages.each do |message|
        flash.now[:warning] << "<li>"+message+"</li>"
      end
      
    elsif state == 'new-request' && type == 'warning'
      flash[:warning] = ''
      user.errors.full_messages.each do |message|
        flash[:warning] << "<li>"+message+"</li>"
      end
      
    end
    
  end
  
  def remember_location
    page = params[:page] ||= "1"
    session[:location] = request.path+"?page="+page
  end
    
end
