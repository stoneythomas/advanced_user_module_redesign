module SessionsHelper
    
    # STARTS A SESSION FOR USER SO THE SITE REMEMBERS THEM DURING A SESSION
    def log_in(user)
      session[:user_id] = user.id
      session[:location] = root_url
      flash.now[:notice] = "<li>Welcome #{current_user.name}</li>"
    end
    
    # REMEMBERS USER FOR SUBSEQUENT NEW SESSIONS AFTER BROWSER CLOSE
    def remember(user)
      user.remember
      cookies.permanent.signed[:user_id] = user.id
      cookies.permanent[:remember_token] = user.remember_token
    end
    
    # HAS THE BROWSER FORGET THE USER SO THEY'LL HAVE TO SIGN IN AGAIN ON NEXT VISIT
    def forget(user)
      user.forget
      cookies.delete(:user_id)
      cookies.delete(:remember_token)
    end
    
    # ENDS THE SESSION FOR THE USER
    def log_out
      forget(current_user)
      session.delete(:user_id)
      @current_user = nil
    end
   
   # LOADS THE CURRENT USER DATA SO WE CAN ACCESS IT ELSEWHERE 
    def current_user
      if user_id = session[:user_id]
        @current_user ||= User.find_by(id: user_id)
      elsif user_id = cookies.signed[:user_id]
        user = User.find_by(id: user_id)
        if user && user.authenticated?(:remember, cookies[:remember_token])
          log_in(user)
          @current_user = user
        end
      end
    end
    
    # TESTS TO SEE IF SOMEONE IS LOGGED IN
    def logged_in?
      !current_user.nil?  
    end
    
    # TEST TO SEE IF SOMEONE IS AN ADMIN
    def is_admin?
      current_user.role == 'admin'
    end
   
   # CHECKS UID TO SEE IF IT MATCHES THE CURRENT USER TO VERIFY PERMISSION TO EDIT ONESELF
   def is_self?(uid = 0)
     return false if session[:user_id].nil?
     session[:user_id] == uid ? true : false  
   end    
  
end
