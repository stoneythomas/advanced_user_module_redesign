class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@stonewall-thomas.com'
  layout 'mailer'
end

