class User < ApplicationRecord
    
    has_secure_password
    has_attached_file :avatar, :styles => { :tiny => "30x30#", :medium => "300x300>", :thumb => "100x100#" }, :default_url => "avatar/:style.jpg"
    has_many :private_messages, :dependent => :destroy
    crop_attached_file :avatar
    before_save :downcase_email
    before_create :create_activation_digest

    attr_accessor :remember_token, :activation_token, :reset_token
    
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    USER_REGEX = /\A^[a-zA-Z0-9_\- ]*$\z/
    
    validates :name,  presence: true, 
                      length: { within: 6..50 },
                      uniqueness: { case_sensitive: false },
                      format: { with: USER_REGEX, :message => 'can only be letters, numbers, dash, space, and underscore' }
                      
    validates :email, presence: true, 
                      length: { maximum: 255 },
                      format: { with: EMAIL_REGEX },
                      uniqueness: { case_sensitive: false }
    
    validates :password, presence: true,
                         length: { :within => 6..50 },
                         :if => :password
                         
    validates :password_confirmation, presence: true,
                                      :if => :password
                                      
    validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"], :message => 'must be jpg, jpeg, png, or gif',
                                               :if => :avatar
                                               
    validates_attachment_size :avatar, :less_than => 3.megabytes,
                                       :if => :avatar
                                       
    # CHANGES EMAILS ADDRESS TO LOWERCASE
    def downcase_email
      self.email = email.downcase
    end
    
    # CREATES TOKEN AND DIGEST FOR PASSWORD RESET PROCEDURE -
    def create_reset_digest
      self.reset_token = User.new_token
      update_attribute(:reset_digest, User.digest(reset_token))
      update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    # CHECKS TO SEE IF PASSWORD RESET IS EXPIRED
    def password_reset_expired?
      reset_sent_at < 2.hours.ago
    end
    
    # CREATES TOKEN AND DIGEST FOR ACTIVATION PROCEDURE
    def create_activation_digest
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    
    # RESETS THE ACTIVATION TOKEN AND DIGEST SO USER CAN TRY AGAIN
    def reset_activation_digest
      self.create_activation_digest
      self.update_attribute(:activation_digest, self.activation_digest)
    end
    
    # ENCRYPTS STRING                                  
    def User.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end
    
    # GENERATES TOKENS                                  
    def User.new_token
      SecureRandom.urlsafe_base64  
    end
    
    # SETS REMEMBER DIGEST IN DB TO REMEMBER USER WHEN THEY RETURN
    def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))
    end
    
    # CHECKS REMEMBER TOKEN AGAINST DB
    def authenticated?(attribute, token)
      digest = send("#{attribute}_digest")
      return false if digest.nil?
      BCrypt::Password.new(digest).is_password?(token)
    end
    
    # FORGETS USER SO THEY'LL HAVE TO LOG IN AGAIN
    def forget
      update_attribute(:remember_digest, nil)
    end
    
end
