class PrivateMessage < ApplicationRecord
    
    attr_accessor :recipient_id
    belongs_to :user
    validates :subject, presence: true
    validates :message, presence: true
    
end



