Rails.application.routes.draw do
  
  get 'password_reset/new'
  get 'password_reset/edit'

  root 'home#index'
  
  resources :users do
    member do
      get :delete
    end 
    member do
      get :resend_activation
    end
    member do
      get :crop_avatar
    end
  end
  
  resources :private_messages, except: [:edit, :update] do 
    member do
      get :delete
    end
    member do
      get :unread
    end
  end
  
  resources :account_activations, only: [:edit]
  
  resources :password_reset, only: [:new, :create, :edit, :update]
  
  get    '/login',  to: 'sessions#new'
  post   '/login',  to: 'sessions#create'
  get    '/logout', to: 'sessions#destroy'

end
