class CreatePrivateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :private_messages do |t|
      t.integer :user_id
      t.integer :sender_id
      t.text :message
      t.datetime :read_date

      t.timestamps
    end
    add_index :private_messages, :user_id
  end
end
