class AddSubjectLineToPrivateMessage < ActiveRecord::Migration[5.0]
  def change
    
    add_column :private_messages, :subject, :string
    
  end
end
