class AddReplyIdColumnToPrivate < ActiveRecord::Migration[5.0]
  def change
    add_column :private_messages, :reply_id, :integer
  end
end
